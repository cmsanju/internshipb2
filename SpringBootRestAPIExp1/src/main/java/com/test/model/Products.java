package com.test.model;

import java.util.ArrayList;
import java.util.List;

public class Products {
	
	private List<Product> prData;

	public List<Product> getPrData() {
		
		if(prData == null)
		{
			prData = new ArrayList<>();
		}
		return prData;
	}

	public void setPrData(List<Product> prData) {
		this.prData = prData;
	}
	
	
}
