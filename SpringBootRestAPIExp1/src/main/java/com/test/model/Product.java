package com.test.model;

public class Product {
	
	private Integer id;
	
	private String name;
	
	private String cmp;
	
	private int price;
	
	public Product()
	{
		
	}
	
	public Product(Integer id, String name, String cmp, int price)
	{
		this.id = id;
		this.name = name;
		this.cmp = cmp;
		this.price = price;
				
	}

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCmp() {
		return cmp;
	}

	public void setCmp(String cmp) {
		this.cmp = cmp;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}
