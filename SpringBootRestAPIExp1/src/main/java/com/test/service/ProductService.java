package com.test.service;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.test.model.Product;
import com.test.model.Products;

@Repository
@Service
public class ProductService {
	
	private static Products prList = new Products();
	
	static
	{
		prList.getPrData().add(new Product(1, "basiclap", "dell", 2345));
		prList.getPrData().add(new Product(2, "macbook", "apple", 5345));
		prList.getPrData().add(new Product(3, "viao", "sony", 8338));
		prList.getPrData().add(new Product(4, "redbook", "asus", 3456));
		prList.getPrData().add(new Product(5, "idapad", "lenovo", 2345));
	}
	
	//read product data
	public Products readProducts()
	{
		return prList;
	}
	
	//add product data
	public void addProduct(Product pr)
	{
		prList.getPrData().add(pr);
	}
	
	//update product data
	public String updateProductData(Product pr)
	{
		for(int i = 0; i < prList.getPrData().size(); i++)
		{
			Product obj = prList.getPrData().get(i);
			
			if(obj.getId().equals(pr.getId()))
			{
				prList.getPrData().set(i, pr);
			}
		}
		
		return "the given id is not available";
	}
	
	//delete product data
	public String deleteProduct(int id)
	{
		for(int i = 0; i < prList.getPrData().size(); i++)
		{
			Product obj = prList.getPrData().get(i);
			
			if(obj.getId().equals(id))
			{
				prList.getPrData().remove(i);
			}
		}
		
		return "the given id is not available";
	}
}
