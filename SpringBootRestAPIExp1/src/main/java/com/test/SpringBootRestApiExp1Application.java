package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestApiExp1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestApiExp1Application.class, args);
	}

}

/*

	1 after gving the path

docker image build -t oct-batch . 

2 start container

docker container run --name octb -p 8080:8080 -d oct-batch

3 for logs
 docker logs octb



*/