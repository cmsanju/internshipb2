package com.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.model.Product;
import com.test.model.Products;
import com.test.service.ProductService;

@RestController
public class ProductController {
	
	@Autowired
	private ProductService service;
	
	@GetMapping(value = "/listproducts", produces = "application/json")
	@ResponseBody
	public ResponseEntity<Products>  getAllProducts()
	{
		
		if(service.readProducts().getPrData().size()>0)
		{
			return new ResponseEntity<Products>(service.readProducts(),HttpStatus.OK);
		}
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		
	}
	
	@PostMapping(value = "/addproduct", consumes = "application/json")
	public ResponseEntity<String> createProduct(@RequestBody Product pr)
	{
		if(pr == null)
		{
			return new ResponseEntity<String>("No Input Data", HttpStatus.NO_CONTENT);
		}
		service.addProduct(pr);
		return new ResponseEntity<String>("Successfully data stored",HttpStatus.CREATED);
	}
	
	@PutMapping(value = "/editproduct/{id}", consumes = "application/json")
	public ResponseEntity<Products> updateProduct(@PathVariable("id") int id, @RequestBody Product pr)
	{
		if(id != 0)
		{
			pr.setId(id);
			service.updateProductData(pr);
			return new ResponseEntity<Products>(service.readProducts(), HttpStatus.ACCEPTED);
		}
		
		return new ResponseEntity<Products>(HttpStatus.NO_CONTENT);
	}
	
	@DeleteMapping(value = "/deleteproduct/{id}", produces = "application/json")
	public ResponseEntity<String> deleteProduct(@PathVariable("id") int id)
	{
		if(id != 0)
		{
			service.deleteProduct(id);
			
			return new ResponseEntity<>("Successfully deleted.", HttpStatus.OK);
		}
		
		return new ResponseEntity<>("id not available in resource", HttpStatus.NO_CONTENT);
	}
}
