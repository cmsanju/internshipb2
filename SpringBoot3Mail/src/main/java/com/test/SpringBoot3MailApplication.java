package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot3MailApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot3MailApplication.class, args);
	}

}
/*
 Emerging technologies:
 
AI 
IOT 
Green Hydrogen 
Tele Medicine 
Advanced Robots 
LIDAR 
Quantum Computing
(Neuromorphic Computing)
Block Chain
DeFI 
Nanotechnology 
Meta materials
Voice User Interface
 
 */
