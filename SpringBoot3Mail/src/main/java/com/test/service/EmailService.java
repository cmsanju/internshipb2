package com.test.service;

import com.test.model.MailData;

public interface EmailService {

    // Method
    // To send a simple email
    String sendSimpleMail(MailData details);

    // Method
    // To send an email with attachment
    String sendMailWithAttachment(MailData details);
}