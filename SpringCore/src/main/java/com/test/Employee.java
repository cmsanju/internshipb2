package com.test;

import org.springframework.beans.factory.annotation.Autowired;

public class Employee {
	
	private int id;
	
	private String name;
	
	private String cmp;
	
	@Autowired
	private Address obj;
	
	public Employee()
	{
		
	}
	
	public Employee(int id, String name, String cmp, Address obj)
	{
		this.id = id;
		this.name = name;
		this.cmp = cmp;
		this.obj = obj;
				
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCmp() {
		return cmp;
	}

	public void setCmp(String cmp) {
		this.cmp = cmp;
	}

	public Address getObj() {
		return obj;
	}

	public void setObj(Address obj) {
		this.obj = obj;
	}
}
