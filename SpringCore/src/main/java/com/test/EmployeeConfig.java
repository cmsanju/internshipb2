package com.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmployeeConfig {
	
	@Bean
	public Address getBeanAdr()
	{
		return new Address();
	}
	
	@Bean
	public Employee getBeanEmp()
	{
		return new Employee();
	}
}
