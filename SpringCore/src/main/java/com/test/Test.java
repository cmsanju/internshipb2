package com.test;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
	
	public static void main(String[] args) {
		
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(EmployeeConfig.class);
		
		Address adr = ctx.getBean(Address.class);
		
		Employee obj = ctx.getBean(Employee.class);
		
		
		adr.setCity("Blr");
		adr.setStreet("ITPL");
		adr.setCntry("INDIA");
		
		obj.setId(101);
		obj.setName("Kohli|Rohit");
		obj.setCmp("ICC");
		obj.setObj(adr);
		
		
		
		System.out.println(obj.getId()+" "+obj.getName()+" "+obj.getCmp());
		System.out.println(obj.getObj().getCity()+" "+obj.getObj().getStreet()+" "+obj.getObj().getCntry());
	}
}
