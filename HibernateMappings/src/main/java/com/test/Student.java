package com.test;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

@Entity
@Table(name = "student")
public class Student {  
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String std_name;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "sid")
	@OrderColumn(name = "type")
	private List<Course> std_crs;
	
	public Student()
	{
		
	}
	
	public Student(int id, String std_name, List<Course> std_crs) {
		this.id = id;
		this.std_name = std_name;
		this.std_crs = std_crs;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStd_name() {
		return std_name;
	}

	public void setStd_name(String std_name) {
		this.std_name = std_name;
	}

	public List<Course> getStd_crs() {
		return std_crs;
	}

	public void setStd_crs(List<Course> std_crs) {
		this.std_crs = std_crs;
	}
}
