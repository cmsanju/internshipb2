package com.test;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class StoreData {
	
	public static void main(String[] args) {
		
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		
		SessionFactory sf = cfg.buildSessionFactory();
		
		Session session = sf.openSession();
		
		Transaction t = session.beginTransaction();
		/*
		Course c1 = new Course();//transient state
		
		c1.setC_name("Full Stack Java");
		
		Course c2 = new Course();
		
		c2.setC_name("MERN Stack");
		
		Course c3 = new Course();
		
		c3.setC_name("AWS");
		
		List<Course> listC = new ArrayList<Course>();
		listC.add(c1);
		listC.add(c2);
		listC.add(c3);
		
		Student std = new Student();//transient state
		
		std.setStd_name("Rahul");
		std.setStd_crs(listC);
		
		session.persist(std);//persistance state
		*/
		
		Customer cst = new Customer();
		
		cst.setC_name("Rohit");
		
		Bank bk = new Bank();
		
		bk.setB_name("PPLBank");
		
		cst.setC_bank(bk);
		bk.setC_data(cst);
		
		session.persist(cst);
		
		t.commit();
		
		session.close();//detached
	}
}
