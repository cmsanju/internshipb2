package com.test;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestDemo {
	
	public static void main(String[] args) {
		/*
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		
		Student obj1 = ctx.getBean("obj", Student.class);
		
		obj1.disp();
		*/
		
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);
		
		Student obj = ctx.getBean(Student.class);
		
		obj.setId(101);
		obj.setName("Dravid");
		
		List<String> sk = new ArrayList<String>();
		
		sk.add("kotlin");
		sk.add("go lang");
		sk.add("ai");
		
		obj.setSkills(sk);
		
		obj.disp();
	}
}
