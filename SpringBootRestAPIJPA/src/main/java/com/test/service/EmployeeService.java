package com.test.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.entity.Employee;
import com.test.repository.EmployeeRepository;

@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeRepository repository;
	
	public List<Employee> getAllEmployees()
	{
		return repository.findAll();
	}
	
	public void addEmployee(Employee emp)
	{
		repository.save(emp);
	}
	
	public Employee updateEmployee(Employee emp, int id)
	{
		emp.setId(id);
		
		return repository.save(emp);
	}
	
	public Employee getByEmployeeId(int id)
	{
		return repository.findById(id).orElse(null);
	}
	
	public void deleteEmployee(int id)
	{
		repository.deleteById(id);
	}
}
