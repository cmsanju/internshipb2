package com.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.entity.Employee;
import com.test.service.EmployeeService;

@RestController
@RequestMapping("/api/v1/employee")
public class EmployeeController {
	
	@Autowired
	private EmployeeService service;
	
	@GetMapping("/list")
	public List<Employee> getAll()//flux
	{
		return service.getAllEmployees();
	}
	
	@PostMapping("/create")
	public void addEmployee(@RequestBody Employee emp)
	{
		service.addEmployee(emp);
	}
	
	@GetMapping("/{id}")
	public Employee getEmployeeById(@PathVariable("id") int id)//mono
	{
		return service.getByEmployeeId(id);
	}
	
	@PutMapping("/update/{id}")
	public Employee updateEmployee(@PathVariable("id") int id, @RequestBody Employee emp)
	{
		return service.updateEmployee(emp, id);
	}
	
	@DeleteMapping("/delete/{id}")
	public void deleteEmployee(@PathVariable("id") int id)
	{
		service.deleteEmployee(id);
	}
}
