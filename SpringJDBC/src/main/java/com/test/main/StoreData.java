package com.test.main;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.test.dao.EmpDao;
import com.test.model.Employee;

public class StoreData {
	
	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		
		EmpDao empd = ctx.getBean("edao", EmpDao.class);
		
		Employee emp = new Employee();
		
		emp.setEmp_id(201);
		emp.setEmp_name("Rohit");
		emp.setEmp_cmp("ICC");
		
		//empd.save(emp);
		//empd.update(emp);
		
		List<Employee> empList =  empd.getEmployees();
		
		for(Employee e : empList)
		{
			System.out.println(e.getEmp_id()+" "+e.getEmp_name()+" "+e.getEmp_cmp());
		}
		
		System.out.println("Done.");
	}
}
