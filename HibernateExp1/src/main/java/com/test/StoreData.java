package com.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class StoreData {
	
	public static void main(String[] args) {
		
		
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		
		SessionFactory sf = cfg.buildSessionFactory();
		
		Session session = sf.openSession();
		
		Transaction t = session.beginTransaction();
		
		Employee emp = new Employee();
		
		emp.setEmp_name("Rohit");
		emp.setEmp_cmp("ICC");
		
		//session.save(emp);
		
		
		
		Employee eobj = session.get(Employee.class, 2);
		
		System.out.println("ID : "+eobj.getId()+" Name : "+eobj.getEmp_name()+" Company : "+eobj.getEmp_cmp());
		
		eobj.setEmp_name("Rahul");
		eobj.setEmp_cmp("CoachBCC");
		
		//session.save(eobj);
		session.remove(eobj);
		t.commit();
		session.close();
	}
}
