package com.test.contoller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.test.model.Student;

@Controller
public class StudentContoller {
	
	//@RequestMapping(value = "/", method = RequestMethod.GET)
	@GetMapping("/")
	public String homePage()
	{
		return "home";
	}
	
	@RequestMapping("/sform")
	public String studentForm(Model model)
	{
		model.addAttribute("std", new Student());
		return "stdform";
	}
	
	@RequestMapping("/submitstd")
	public String submitStudent(
			@Valid @ModelAttribute("std") Student std, Model model,
			BindingResult result)
	{
		
		
		if(result.hasErrors())
		{
			return "stdform";
		}
		
		return "stddata";
	}
}
