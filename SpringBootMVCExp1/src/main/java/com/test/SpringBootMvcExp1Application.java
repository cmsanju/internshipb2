package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMvcExp1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMvcExp1Application.class, args);
	}

}
