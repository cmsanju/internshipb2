package com.test.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

public class Student {
	
	@NotBlank(message = "ID required")
	private int id;
	
	@NotEmpty(message = "Name required")
	private String name;
	
	@NotEmpty(message = "College name required")
	private String clg;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClg() {
		return clg;
	}

	public void setClg(String clg) {
		this.clg = clg;
	}
}
