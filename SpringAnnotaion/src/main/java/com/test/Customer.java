package com.test;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

public class Customer {
	
	private int id;
	
	private String name;
	
	@Autowired
	private List<Address> adr;
	
	private Map<String, Integer> product;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Address> getAdr() {
		return adr;
	}

	public void setAdr(List<Address> adr) {
		this.adr = adr;
	}

	public Map<String, Integer> getProduct() {
		return product;
	}

	public void setProduct(Map<String, Integer> product) {
		this.product = product;
	}
}
