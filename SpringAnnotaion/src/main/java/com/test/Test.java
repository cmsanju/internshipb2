package com.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
	
	public static void main(String[] args) {
		
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);
		
		Product obj =  ctx.getBean(Product.class);
		
		List<Address> adrData = new ArrayList<>();
		Customer cst = ctx.getBean(Customer.class);
		Address adr1 = new Address();
		adr1.setState("KA");
		adr1.setCnt("india");
		Address adr2 = new Address();
		adr2.setState("AP");
		adr2.setCnt("india");
		
		
		adrData.add(adr1);
		adrData.add(adr2);
		
		cst.setId(111);
		cst.setAdr(adrData);
		cst.setName("Rohit");
		
		Map<String, Integer> prData = new HashMap<>();
		
		prData.put("Sony", 123);
		prData.put("Dell", 345);
		prData.put("Asus", 645);
		
		
		cst.setProduct(prData);
		
		/*
		List<String> prNames = new ArrayList<String>();
		
		prNames.add("Table");
		prNames.add("Chair");
		prNames.add("Bed");
		prNames.add("home needs");
		
		obj.setId(101);
		obj.setName(prNames);
		obj.setPrice(123);
		
		System.out.println("ID : "+obj.getId()+" Name : "+obj.getName()+" "+obj.getPrice());
		*/
		
		for(Address adr : adrData)
		{
		
		System.out.println(cst.getId()+" "+cst.getName()+" "+ adr.getState()+" "+adr.getCnt()+" "+cst.getProduct());
		}
	}
}
