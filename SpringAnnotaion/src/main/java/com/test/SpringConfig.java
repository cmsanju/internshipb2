package com.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfig {
	
	@Bean
	public Product getObjPr()
	{
		return new Product();
	}
	
	@Bean
	public Address getObjAdr()
	{
		return new Address();
	}
	
	@Bean
	public Customer getObjCst()
	{
		return new Customer();
	}
}
