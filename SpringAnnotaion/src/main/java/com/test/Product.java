package com.test;

import java.util.List;

public class Product {
	
	private int id;
	
	private List<String> name;
	
	private int price;
	
	
	
	public Product()
	{
		
	}

	public Product(int id, List<String> name, int price) {
		
		this.id = id;
		this.name = name;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<String> getName() {
		return name;
	}

	public void setName(List<String> name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}
