package com.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.entity.Employee;
import com.test.entity.Employees;
import com.test.service.EmployeeService;

@RestController// ResponseBody and Controller
public class EmployeeController {
	
	@Autowired
	private EmployeeService empService;
	
	@GetMapping("/listemployees")
	public Employees getAllEmployees()
	{
		return empService.getAllEmployees();
	}
	
	@PostMapping("/createemployee")
	public Employees createEmployee(@RequestBody Employee emp)
	{
		int id = empService.getAllEmployees().getEmp().size()+1;
		
		emp.setId(id);
		empService.addEmployee(emp);
		
		return empService.getAllEmployees();
	}
	
	@PutMapping("/updateemployee/{id}")
	public Employees updateEmployee(@RequestBody Employee emp, @PathVariable("id") Integer id)
	{
		emp.setId(id);
		
		empService.updateEmp(emp);
		
		return empService.getAllEmployees();
	}
	
	@DeleteMapping("/deleteemployee/{id}")
	public Employees deleteEmployee(@PathVariable("id") Integer id)
	{
		empService.deleteEmp(id);
		
		return empService.getAllEmployees();
	}
}
