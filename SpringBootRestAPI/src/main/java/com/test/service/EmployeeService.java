package com.test.service;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.test.entity.Employee;
import com.test.entity.Employees;

@Repository
@Service
public class EmployeeService {
	
	private static Employees listEmp = new Employees();
	
	static
	{
		listEmp.getEmp().add(new Employee(1, "Venkates", "HCL"));
		listEmp.getEmp().add(new Employee(2, "Jeeva", "perfs"));
		listEmp.getEmp().add(new Employee(3, "Kshma", "Dell"));
		listEmp.getEmp().add(new Employee(4, "Rahul", "ICC"));
		listEmp.getEmp().add(new Employee(5, "Rohit", "BCCI"));
		
	}
	//adding the employee data
	public void addEmployee(Employee emp)
	{
		listEmp.getEmp().add(emp);
		
	}
	
	//displaying the employee data
	public Employees getAllEmployees()
	{
		return listEmp;
	}
	
	//updating the employee data based on id
	public String updateEmp(Employee emp)
	{
		for(int i = 0; i < listEmp.getEmp().size(); i++)
		{
			Employee emp1 = listEmp.getEmp().get(i);
			
			if(emp1.getId().equals(emp.getId()))
			{
				listEmp.getEmp().set(i, emp);
			}
		}
		
		return "the given id is not available";
	}
	
	//deleting the employee based on id
	public String deleteEmp(Integer id)
	{
		for(int i = 0; i<listEmp.getEmp().size(); i++)
		{
			Employee emp = listEmp.getEmp().get(i);
			
			if(emp.getId().equals(id))
			{
				listEmp.getEmp().remove(i);
			}
		}
		
		return "the given id is not available";
	}
}
