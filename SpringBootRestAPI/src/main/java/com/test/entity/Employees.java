package com.test.entity;

import java.util.ArrayList;
import java.util.List;

public class Employees {
	
	private List<Employee> emp;

	public List<Employee> getEmp() {
		
		if(emp == null)
		{
			emp = new ArrayList<>();
		}
		
		return emp;
	}

	public void setEmp(List<Employee> emp) {
		this.emp = emp;
	}
}
