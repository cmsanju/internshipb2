package com.test.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.test.model.User;

@Controller
public class UserController {
	
	@RequestMapping("/user")
	public String userForm()
	{
		return "reg";
	}
	/*
	@RequestMapping("/subbitdata")
	public String submitForm(HttpServletRequest request, Model model)
	{
		String usr = request.getParameter("user");
		String pas = request.getParameter("pwd");
		
		User obj = new User();
		
		obj.setUserName(usr);
		obj.setPassword(pas);
		
		model.addAttribute("info",obj);
		
		return "userdata";
	}
	
	*/
	
	@RequestMapping("/subbitdata")
	public String submitForm(@RequestParam("user") String usr, @RequestParam("pwd")String pas, Model model)
	{
		//String usr = request.getParameter("user");
		//String pas = request.getParameter("pwd");
		
		User obj = new User();
		
		obj.setUserName(usr);
		obj.setPassword(pas);
		
		model.addAttribute("info",obj);
		
		return "userdata";
	}
}
