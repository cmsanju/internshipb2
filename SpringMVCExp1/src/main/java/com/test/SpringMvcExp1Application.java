package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcExp1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringMvcExp1Application.class, args);
	}

}
