package com.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
	
	@RequestMapping("/")
	public String defaultPage()
	{
		return "index";
	}
	
	@RequestMapping("/hdata")
	public String homePageData()
	{
		return "home";
	}
}
